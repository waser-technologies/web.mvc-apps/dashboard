# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
import os

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    path('sitemap\.xml', sitemap, {'sitemaps': {'cmspages': CMSSitemap}}),
    path('taggit_autosuggest/', include('taggit_autosuggest.urls')),
]

if settings.USE_I18N:
    urlpatterns += i18n_patterns(
        path('admin/', admin.site.urls),
        path('accounts/', include('allauth.urls')),
        path('dashboard/', include('dashboard.urls'))
    )
else:
    urlpatterns += [
        path('admin/', admin.site.urls),
        path('accounts/', include('allauth.urls')),
        path('dashboard/', include('dashboard.urls'))
    ]

""" if settings.DS_SETTINGS != None:
    for urlpattern in settings.DS_SETTINGS.get('urlpatterns', []):
        if urlpattern['include'].get('namespace', False):
            urlpatterns.append(re_path(urlpattern.get('url'), include(urlpattern['include'].get('module'), namespace=urlpattern['include'].get('namespace'))))
        else:
            urlpatterns.append(re_path(urlpattern.get('url'), include(urlpattern['include'].get('module'))))
 """
urlpatterns += [
        re_path(r'^', include('cms.urls'))
    ]

# This is only needed when using runserver.
if os.environ.get('DEV', False):
    urlpatterns = [
        path('media/', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns