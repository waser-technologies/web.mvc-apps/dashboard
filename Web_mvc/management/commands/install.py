from django.core.management.base import BaseCommand, CommandError
import os
import pickle
from django.conf import settings
from django.conf.urls import include, url

class Command(BaseCommand):
    help = 'Install specified app inside dynamicly stored settings (DSS)'

    def add_arguments(self, parser):
        parser.add_argument('app_names', nargs=1, type=str)
        parser.add_argument(
            '--url',
            action='store_true',
            help='Use this flag if app needs a url',
        )
        parser.add_argument(
            '--namespace',
            action='store_true',
            help='Use this flag if app needs a namespace',
        )
        parser.add_argument(
            '--app_name',
            action='store_true',
            help='Use this flag if app needs an app_name',
        )

    def handle(self, *args, **options):
        main_settings_path = os.path.join(settings.BASE_DIR, settings.DS_SETTINGS_FILENAME)
        if not os.path.isfile(main_settings_path):
            DS_SETTINGS = {'urlpatterns':[], 'load_dss':[]}
        else:
            main_dss_file = open(main_settings_path, 'rb')
            DS_SETTINGS = pickle.load(main_dss_file)
            main_dss_file.close()
            if not DS_SETTINGS.get('urlpatterns', None):
                DS_SETTINGS['urlpatterns'] = []

        for app in options['app_names']:
            DS_SETTINGS['load_dss'] += [app + "/settings.dss"]
            if options['url']:
                urlpattern = {'url':r'{0}/'.format(app), 'include':{'module':"{0}.urls".format(app)}}
                if options['namespace']:
                    urlpattern['include']['namespace'] = app
                if options['app_name']:
                    urlpattern['include']['app_name'] = app

                DS_SETTINGS['urlpatterns'].append(urlpattern)
        
        file = open(os.path.join(settings.BASE_DIR, settings.DS_SETTINGS_FILENAME), 'wb')
        pickle.dump(DS_SETTINGS, file)
        file.close()
        self.stdout.write(self.style.SUCCESS('Successfully installed app'))
