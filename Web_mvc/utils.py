import os
import pickle

def module_path(module_name):
    module = __import__(module_name.split(".")[0])
    path = os.path.dirname(module.__file__)
    return path

def load_dss(BASE_DIR, main_dss):
    main_dss_absolute = os.path.join(BASE_DIR, main_dss)
    if os.path.isfile(main_dss_absolute):
        DS_SETTINGS_FILE = open(main_dss_absolute, 'rb')
        DSS = pickle.load(DS_SETTINGS_FILE)
        DS_SETTINGS_FILE.close()

        for dss_src in DSS.get('load_dss'):
            dss_src_absolute = os.path.join(module_path(dss_src.split("/")[0]), dss_src.split("/")[1])
            if os.path.isfile(dss_src_absolute):
                DSS_LOAD = open(dss_src_absolute, 'rb')
                dss = pickle.load(DSS_LOAD)

            DSS_LOAD.close()
            for setting in dss.get('settings').keys():
                if DSS['settings'].get(setting, None):
                    if dss['settings'].get(setting) is list and dss['settings'][setting].get(key) is dict:
                        for key in dss['settings'].get(setting):
                            DSS['settings'][setting][-1][key] += dss['settings'][key]
                    elif dss['settings'].get(setting) is list:
                        DSS['settings'][setting] += dss['settings'].get(setting)
                    elif dss['settings'].get(setting) is dict:
                        for key in dss['settings'].get(setting):
                            DSS['settings'][setting][key] = dss['settings'][key]
                    else:
                        DSS['settings'][setting] = dss['settings'].get(setting)
                else:
                    DSS['settings'][setting] = dss['settings'].get(setting)
    else:
        DSS = None
    return DSS