include ./.env

env:
	pip install -r './requirements.txt'
	pip install -r './requirements-git.txt'

clone:
	git clone $(GIT_PATH) .

pull:
	git pull $(GIT_PATH)

static:
	python ./manage.py collectstatic --no-input

init:
	python ./init_dss.py
	python ./manage.py migrate --no-input
	python ./manage.py collectstatic --no-input
	python ./manage.py createsuperuser

migrations:
	python ./manage.py makemigrations --no-input

migrate:
	python ./manage.py migrate --no-input

server:
	python ./manage.py runserver

messages:
	python ./manage.py makemessages -l fr --ignore=static/* --ignore=env/*

translate:
	python ./manage.py compilemessages -l fr

sass:
	sass --watch static/scss:static/scss

make dss:
	python ./init_dss.py