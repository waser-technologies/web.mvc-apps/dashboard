from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import DashboardStatistics, DashboardAccountStatistic
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

__all__ = ['DashboardAccountStatisticPlugin']

@plugin_pool.register_plugin
class DashboardStatisticsPlugin(CMSPluginBase):
    model = DashboardStatistics
    name = _("Statistics")
    module = _("Dashboard")
    render_template = "dashboard/plugins/statistics.html"
    cache = False
    allow_children = True
    child_classes = __all__

    def render(self, context, instance, placeholder):
        context = super(DashboardStatisticsPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class DashboardAccountStatisticPlugin(CMSPluginBase):
    model = DashboardAccountStatistic
    name = _("Users")
    module = _("Dashboard Statistics")
    render_template = "dashboard/plugins/users.html"
    cache = False
    require_parent = True
    parent_classes = ['DashboardStatisticsPlugin']

    def render(self, context, instance, placeholder):
        context = super(DashboardAccountStatisticPlugin, self).render(context, instance, placeholder)
        User = get_user_model()
        context['users'] = User.objects.all()
        context['users_increase'] = 12
        return context