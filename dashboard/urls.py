from __future__ import absolute_import, print_function, unicode_literals

from django.conf.urls import url
from django.contrib.admin.views.decorators import staff_member_required
from dashboard.views import DashboardMainView, Dashboard
from dashboard.models import DashboardPage


app_name = "dashboard"

urlpatterns = [
    url(r'^$', Dashboard, name="dashboard-main"),
    url(r'^(?P<slug>[\w\.@+-]+)/', staff_member_required(DashboardMainView.as_view()), name="dashboard-detail"),
]
