from django.shortcuts import render, redirect, reverse
from django.views.generic import DetailView
from dashboard.models import DashboardPage
from parler.views import TranslatableSlugMixin

# Create your views here.
class BaseDashboardView(TranslatableSlugMixin, DetailView):
    model = DashboardPage

    def get_queryset(self):
        try:
            return super().get_queryset()
        except Exception as e:
            return DashboardPage.objects.filter(user=self.request.user).first()

class DashboardMainView(BaseDashboardView):
    template_name = "dashboard/default.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = DashboardPage.objects.filter(user=self.request.user).first()
        context['active_id'] = page.pk
        context['dashboard_content'] = page
        context['dashboard_sidebar'] = DashboardPage.objects.filter(user=self.request.user).all()
        return context

def Dashboard(request):
    dashboard = DashboardPage.objects.filter(user=request.user).first()
    if dashboard != None:
        url = reverse("dashboard:dashboard-detail", kwargs={'slug': dashboard.slug})
        return redirect(url)
    else:
        return render(request, "dashboard/default.html")
