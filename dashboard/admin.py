from django.contrib import admin
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin

from .models import DashboardPage

# Register your models here.
class DashboardPageAdmin(PlaceholderAdminMixin, FrontendEditableAdminMixin, TranslatableAdmin):
    frontend_editable_fields = ("page")

admin.site.register(DashboardPage, DashboardPageAdmin)