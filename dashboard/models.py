from django.db import models
from cms.models import PlaceholderField
from parler.models import TranslatableModel, TranslatedFields
from djangocms_icon.fields import Icon
from django.utils.translation import ugettext_lazy as _
from django.conf import settings 
from cms.plugin_base import CMSPlugin

User = settings.AUTH_USER_MODEL

# Create your models here.
class DashboardPage(TranslatableModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    translations = TranslatedFields(
        name=models.CharField(max_length=38),
        slug=models.SlugField(max_length=38),
    )
    icon = Icon()
    order = models.IntegerField(verbose_name=_("Order"), default=1)
    page=PlaceholderField('dashboard_content')

class DashboardStatistics(CMSPlugin):
    pass

class DashboardAccountStatistic(CMSPlugin):
    pass